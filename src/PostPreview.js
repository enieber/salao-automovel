import React from 'react';

//const PostPreview = () => {

class PostPreview extends React.Component {
  render() {
    return (
    <div>
      <img alt={this.props.img.alt} src={this.props.img.src} />	
      <h2>{this.props.title}</h2>
      <p>{this.props.date}</p>
      <p>{this.props.intro}</p>
    </div>
    )
  }
}

export default PostPreview;

