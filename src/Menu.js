import React from "react"
import styled from "styled-components"

const MenuStyled = styled.nav`
  color:#E33409;
  font-size:1.5em;
  background:#629F47;
  padding:12px;
`;

const UlStyled = styled.ul`
  list-style: none;
  display: flex;
  justify-content: space-around;
`;

const AStyled = styled.a`
  text-decoration: none;
  color: #ECE7DC;
`;


const Menu =()=>{
  return (
    <MenuStyled>
      <UlStyled>
	<li>
	  <AStyled href="#noticias">Noticias</AStyled>
	</li>
	<li>
	  <AStyled href="#marcas">Marcas </AStyled>
	</li>
      </UlStyled>
    </MenuStyled>
  )
}

export default  Menu;

