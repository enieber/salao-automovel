import React from "react";
import styled from "styled-components";

const HeaderStyled = styled.header`
  color: #E33409;
  text-align: center;
  background:#151311;
  padding: 9px;
`;

const Header = () => {
  return (
    <HeaderStyled>
     <h1> Salão Do Automovel </h1>
    </HeaderStyled>
  )
}

export default Header;

