import React, { Component } from 'react';
import Header from "./Header";
import Menu from "./Menu";
import PostPreview from './PostPreview';

class App extends Component {
  render() {
    return (
      <div className="App">
	<Header/>
	<Menu/>
	<PostPreview
	  title={"Avião ou Carro?"}
	  date={"19/11/2018"}
	  intro={"Com a quantidade de Carros hoje é legal viajar de avião?"}
	  img={{
	    alt: "avião em voo",
	    src: "https://raw.githubusercontent.com/enieber/enieber.github.io/master/img/a.jpg"
	  }}
	/>

	<PostPreview
	  title={"Jeep Compass"}
	  date={"19/11/2018"}
	  intro={"Um novo carro compacto da Jeep"}
	  img={{
	    alt: "carro em movimento de cor laranja",
	    src: "https://abrilquatrorodas.files.wordpress.com/2017/11/qr-701-carro-compara-equinox-02.jpg"
	  }}
	/>
      </div>
    );
  }
}

export default App;
